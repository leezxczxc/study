/**
 * Created by SeHeon on 2016-12-22.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import Hello2 from '../components/Hello2';

ReactDOM.render(
    <Hello2 />,
    document.getElementById('hello2')
);
