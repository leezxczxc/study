/**
 * Created by SeHeon on 2016-12-22.
 */
import React from 'react';
import { Link } from 'react-router';

class App extends React.Component {
    render(){
        return (
            <div>
                <ul>
                    <li><Link to="home">Home</Link></li>
                    <li><Link to="about">About</Link></li>
                    <li><Link to="articles">Articles</Link></li>
                </ul>
                {this.props.children}
            </div>
        );
    }
}

class Home extends React.Component {
    render(){
        return (
            <h2>Hey, I am HOME!</h2>
        );
    }
}
class About extends React.Component{
    render(){
        return (
            <h2>Hey, I am ABOUT!</h2>
        );
    }
}
class Articles extends React.Component{
    render(){
        return (
            <h2>Hey, I am ARTICLES!</h2>
        );
    }
}
export default App;
export {
    Home, About, Articles
}