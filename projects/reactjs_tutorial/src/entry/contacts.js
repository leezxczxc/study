/**
 * Created by SeHeon on 2016-12-22.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App2 from '../components/App2';

const contactsElement = document.getElementById('contacts');
ReactDOM.render
(
    <App2 />
    ,contactsElement
);