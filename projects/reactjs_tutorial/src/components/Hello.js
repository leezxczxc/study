/**
 * Created by SeHeon on 2016-12-22.
 */
import React from 'react';

class Hello extends React.Component {
    render(){
        return (
          <div>
              <input ref={ref => this.input = ref}></input>
          </div>
        );
    }
    componentDidMount(){
        this.input.value = "Hi, I used ref to do this";
    }
}
export default Hello;