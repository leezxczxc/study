/**
 * Created by SeHeon on 2016-12-22.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import Hello from '../components/Hello';

ReactDOM.render(
    <Hello />,
    document.getElementById('hello')
);
