/**
 * Created by SeHeon on 2016-08-05.
 */
var app = angular.module('myApp', []);
app.controller('namesCtrl', function($scope){
    $scope.names = [
        {name: 'John Doe', country: 'USA'}
        ,{name: 'Cristiano Ronaldo', country: 'Portugal'}
        ,{name: 'Alan Smith', country: 'UK'}
    ]
});