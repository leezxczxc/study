/**
 * Created by SeHeon on 2016-05-01.
 */
angular.module("customFilters", [])
.filter("unique", function(){
    return function(data, propertyName){
        if(angular.isArray(data) && angular.isString(propertyName)){
            var results = []
            var keys = {}
            for(var i = 0; i < data.length; i++){
                var val = data[i][propertyName]
                if(angular.isUndefined(keys[val])){
                    keys[val] = true
                    results.push(val)
                }
            } // end of for
            return results
        } // end of if(angular.isArray(data) && angular.isString(propertyName))
        else{
            return data
        }
    }   // end of return
}) // end of .filter(
.filter("range", function($filter){
    return function(data, page, size){
        if(angular.isArray(data) && angular.isNumber(page) && angular.isNumber(size)){
            var start_idx = (page - 1) * size
            if(data.length < start_idx){
                return []
            }else{
                return $filter("limitTo")(data.splice(start_idx), size)
            }
        }else{
            return data
        }
    }
})
.filter("pageCount", function(){
    return function(data, size){
        if(angular.isArray(data)){
            var result = []
            for(var i = 0 ; i < Math.ceil(data.length / size); i++){
                result.push(i)
            }
            return result
        }else{
            return data
        }
    }
})

