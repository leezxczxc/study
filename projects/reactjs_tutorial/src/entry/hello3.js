/**
 * Created by SeHeon on 2016-12-22.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import Hello3 from '../components/Hello3';

ReactDOM.render(
    <Hello3 />,
    document.getElementById('hello3')
);
