webpackJsonp([1,0],{

/***/ "./dev/components/HelloWorld.jsx":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = __webpack_require__("./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// createClass and PropTypes has been deprecated in v15.5
/* var HelloWorld = React.createClass({
    render: function(){
        return (
            <p>Hello, {this.propTypes.greetTarget}!</p>
        );
    }
}); */

var HelloWorld = function HelloWorld(_ref) {
    var greetTarget = _ref.greetTarget;

    return _react2.default.createElement(
        "p",
        null,
        "Hello, ",
        greetTarget,
        "!"
    );
};

exports.default = HelloWorld;

/***/ }),

/***/ "./dev/index.jsx":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__("./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__("./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _HelloWorld = __webpack_require__("./dev/components/HelloWorld.jsx");

var _HelloWorld2 = _interopRequireDefault(_HelloWorld);

var _AlertButton = __webpack_require__("./dev/components/AlertButton.jsx");

var _AlertButton2 = _interopRequireDefault(_AlertButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_React$Component) {
    _inherits(App, _React$Component);

    function App() {
        _classCallCheck(this, App);

        return _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this));
    }

    _createClass(App, [{
        key: "render",
        value: function render() {
            return _react2.default.createElement(
                "div",
                null,
                _react2.default.createElement(_HelloWorld2.default, { greetTarget: "Babysoul" }),
                _react2.default.createElement(_AlertButton2.default, { greetTarget: "Babysoul" }),
                _react2.default.createElement(_HelloWorld2.default, { greetTarget: "Yoo Ji-Ae" }),
                _react2.default.createElement(_HelloWorld2.default, { greetTarget: "Seo Ji-Soo" }),
                _react2.default.createElement(_HelloWorld2.default, { greetTarget: "Lee Mi-Joo" }),
                _react2.default.createElement(_HelloWorld2.default, { greetTarget: "Kei" }),
                _react2.default.createElement(_HelloWorld2.default, { greetTarget: "Jin" }),
                _react2.default.createElement(_HelloWorld2.default, { greetTarget: "Ryo Soo-Jeong" }),
                _react2.default.createElement(_HelloWorld2.default, { greetTarget: "Jeong Ye-In" })
            );
        }
    }]);

    return App;
}(_react2.default.Component);

;

_reactDom2.default.render(_react2.default.createElement(App, null), document.querySelector("#container"));

/***/ })

},["./dev/index.jsx"]);