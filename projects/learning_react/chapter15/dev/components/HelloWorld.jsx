import React from "react";

// createClass and PropTypes has been deprecated in v15.5
/* var HelloWorld = React.createClass({
    render: function(){
        return (
            <p>Hello, {this.propTypes.greetTarget}!</p>
        );
    }
}); */

const HelloWorld = ({greetTarget}) => {
    return (
        <p>Hello, {greetTarget}!</p>
    );
};

export default HelloWorld;