var webpack = require("webpack");
var path = require("path");

var DEV = path.resolve(__dirname, "dev");
var OUTPUT = path.resolve(__dirname, "output");

// 최적화 및 읽기 힘들게 해줌
const uglifyP = new webpack.optimize.UglifyJsPlugin();
// 자동으로 공통 모듈만 모아 줌
const extractCommons = new webpack.optimize.CommonsChunkPlugin({
    name: 'commons',
    filename: 'commons.js'
});
//브라우저에서 HMR 에러발생시 module name 표시
const namedModulesP = new webpack.NamedModulesPlugin();
//발생 횟수에 따라서 모듈 및 청크 id를 할당
const occrruenceOrderP = new webpack.optimize.OccurrenceOrderPlugin();

var config = {
    // 각 모듈 별로 js파일 만들어 줌.( name: module path )
    entry: {
        idx1: DEV + "/index.jsx",
        idx2: DEV + "/index2.jsx"
    },
    // 결과 파일 위치
    output: {
        path: OUTPUT,
        filename: "[name].bundle.js"
    },
    plugins: [
        uglifyP,
        extractCommons,
        namedModulesP,
        occrruenceOrderP
    ],
    // 번들링 시 처리해야할 task 설정
    module: {
        rules: [{   // 번들링 시 사용할 loader 설정
            test: /\.jsx$/, // load할 파일 지정
            include: DEV,   // 번들링할 파일들 위치
            exclude: '/node_modules/',  // 번들링에서 제외할 파일
            use: [{ // 번들링에 사용되는 loader 목록 및 사전설정 모듈
                loader: 'babel-loader', // 사용할 loader
                options: {  // 번들링 옵션
                    presets: [  // 사전설정 모듈
                        ['babel-preset-es2015', 'babel-preset-react']
                    ]
                }
            }]
        }]
        /* loaders: [{
            include: DEV,
            loader: "babel-loader"
        }] */
    },
    resolve: {
        modules: ['node_modules/'],
        descriptionFiles: ['.babelrc'],
        extensions : ['.js', '.jsx', 'ts']
    }
};


module.exports = config;