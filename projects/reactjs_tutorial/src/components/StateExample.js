/**
 * Created by SeHeon on 2016-12-21.
 */
import Reacat from 'react';

class StateExample extends Reacat.Component{
    constructor(props){
        super(props);

        this.state = {
            header: 'Header Initial State',
            content: 'Content initial State'
        };
    }
    updateHeader(text){
        this.setState({
            header: 'Header has changed'
        })
    }
    render(){
        return (
            <div>
                <h1>{this.state.header}</h1>
                <h2>{this.state.content}</h2>
                <button onClick={this.updateHeader.bind(this)}>Update</button>
            </div>
        )
    }
}
export default StateExample;