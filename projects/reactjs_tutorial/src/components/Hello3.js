/**
 * Created by SeHeon on 2016-12-22.
 */
import React from 'react';

class Hello3 extends React.Component {
    handleClick(){
        this.input.value = "";
        this.input.focus();
    }
    render(){
        return (
            <div>
                <input ref={ref => this.input = ref} />
                <button onClick={this.handleClick.bind(this)}>Click Me</button>
            </div>
        );
    }
}
export default Hello3;