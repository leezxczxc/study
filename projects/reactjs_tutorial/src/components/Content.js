/**
 * Created by SeHeon on 2016-12-21.
 */
import React from 'react';
class Content extends React.Component{
    render(){
        return (
            <div>
                <h2>{this.props.title}</h2>
                <p>{this.props.body}</p>
            </div>
        )
    }
}
Content.propsTypes = {
    title: React.PropTypes.string,
    body: React.PropTypes.string.isRequired
}

export default Content;