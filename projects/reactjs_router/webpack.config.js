/**
 * Created by SeHeon on 2016-12-22.
 */


var defaultCfg = {
    entry: {
        index: './src/entry/index'
    },

    output: {
        path: '/public/'
        ,filename: '[name].bundle.js'
        ,chunkFilename: '[id].chunk.js'
    },

    devServer: {
        inline: true,
        port: 7777,
        contentBase: __dirname + '/public/',
        historyApiFallback: true
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react']
                }
            }
        ]
    }
};

module.exports = [
    defaultCfg
];