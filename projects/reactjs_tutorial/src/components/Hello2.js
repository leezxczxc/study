/**
 * Created by SeHeon on 2016-12-22.
 */
import React from 'react';

class Hello2 extends React.Component {
    handleClick(){
        this.textBox.input.value = 'I used ref';
    }
    render(){
        return (
            <div>
                <TextBox ref={ref=>this.textBox = ref}/>
                <button onClick={this.handleClick.bind(this)}>Click Me</button>
            </div>
        );
    }
}
class TextBox extends React.Component {
    render(){
        return(
            <input ref={ref => this.input = ref}></input>
        );
    }
}

export default Hello2;