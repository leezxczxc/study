/**
 * Created by SeHeon on 2016-12-21.
 */
//var path = require('path');
var defaultCfg = {
    devServer: {
        inline: true,
        port: 7777,
        contentBase: __dirname + '/public/'
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react']
                }
            }
        ]
    }
}
var rootCfg = Object.assign({}, defaultCfg, {
    entry: {
        index: './src/entry/index'
        ,contacts: './src/entry/contacts'
        ,hello: './src/entry/hello'
        ,hello2: './src/entry/hello2'
        ,hello3: './src/entry/hello3'
    },

    output: {
        //path: path.join(__dirname, 'js'),
        path: '/public/',
        filename: '[name].bundle.js',
        chunkFilename: '[id].chunk.js'
    }
});
module.exports = [
    rootCfg
]