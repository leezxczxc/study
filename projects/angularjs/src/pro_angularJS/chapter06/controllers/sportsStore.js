// angularjs의 module 정의법: angular.module("name", array) -> array에는 배열이 들어감(빈 배열인 []도 가능)
//두 번째 파라미터가 지정되지 않았으므로 기존에 만들어진 module을 찾음
angular.module("sportsStore")
.controller("sportsStoreCtrl", function($scope){
    $scope.data = {
        products: [
            {
                name: "Product #1",
                description: "A product",
                category: "Category #1",
                price: 100
            },
            {
                name: "Product #2",
                description: "A product",
                category: "Category #1",
                price: 110
            },
            {
                name: "Product #3",
                description: "A product",
                category: "Category #2",
                price: 210
            },
            {
                name: "Product #4",
                description: "A product",
                category: "Category #3",
                price: 202
            }
        ]
    }
})