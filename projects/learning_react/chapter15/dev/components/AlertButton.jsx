import React,{Component, PropTypes} from "react";

class AlertButton extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            bDo: false,
            something: 'do something'
        }
    }
    showGreet(e){
        alert("Hi~ " + this.props.greetTarget + ", " + this.state.something)
        if( this.state.bDo ){
            this.setState({something: 'do something', bDo: false});
        }
        else{
            this.setState({something: 'sing a song', bDo: true});
        }
    }
    render(){
        return (
            <button onClick={this.showGreet.bind(this)}>{this.props.greetTarget}</button>
        );
    }
};

export default AlertButton;