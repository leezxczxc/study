import React from "react";
import ReactDOM from "react-dom";
import AlertButton from "./components/AlertButton"

// createClass and PropTypes has been deprecated in v15.5
class HelloWorld2 extends React.Component{
    constructor(){
        super();
    }
    render(){
        return (
            <p>Hello, {this.props.greetTarget}!</p>
        );
    }
};

ReactDOM.render(
    <div>
        <HelloWorld2 greetTarget="Babysoul" /><AlertButton greetTarget="Babysoul" />
        <HelloWorld2 greetTarget="Yoo Ji-Ae" />
        <HelloWorld2 greetTarget="Seo Ji-Soo" />
        <HelloWorld2 greetTarget="Lee Mi-Joo" />
        <HelloWorld2 greetTarget="Kei" />
        <HelloWorld2 greetTarget="Jin" />
        <HelloWorld2 greetTarget="Ryo Soo-Jeong" />
        <HelloWorld2 greetTarget="Jeong Ye-In" />
    </div>,
    document.querySelector("#container")
);