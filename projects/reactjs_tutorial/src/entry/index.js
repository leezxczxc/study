/**
 * Created by SeHeon on 2016-12-21.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App';

const rootElement = document.getElementById('root');
ReactDOM.render
(
    <App  />
    /*<App headerTitle="Welcome!" contentTitle="Stranger, " contentBody="Welcome to example app" />*/
    ,rootElement
);