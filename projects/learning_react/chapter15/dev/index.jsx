import React from "react";
import ReactDOM from "react-dom";
import HelloWorld from "./components/HelloWorld"
import AlertButton from "./components/AlertButton"

class App extends React.Component{
    constructor(){
        super();
    }
    render(){
        return (
            <div>
                <HelloWorld greetTarget="Babysoul" /><AlertButton greetTarget="Babysoul" />
                <HelloWorld greetTarget="Yoo Ji-Ae" />
                <HelloWorld greetTarget="Seo Ji-Soo" />
                <HelloWorld greetTarget="Lee Mi-Joo" />
                <HelloWorld greetTarget="Kei" />
                <HelloWorld greetTarget="Jin" />
                <HelloWorld greetTarget="Ryo Soo-Jeong" />
                <HelloWorld greetTarget="Jeong Ye-In" />
            </div>
        );
    }
};

ReactDOM.render(
    <App />,
    document.querySelector("#container")
);
