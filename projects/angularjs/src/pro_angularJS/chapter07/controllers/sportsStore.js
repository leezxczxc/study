/**
 * Created by SeHeon on 2017-02-25.
 */

angular.module("sportsStore")
.constant("dataUrl", "http://localhost:8888/sample/list")
.controller("sportsStoreCtrl", function( $scope, $http, dataUrl){

    $scope.data = {};

    $http.get(dataUrl)
    .success(function(data){
        $scope.data.products = data;
    })
    .error(function(error){
        $scope.data.error = error;
    });
});