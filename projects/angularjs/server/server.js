// 2.x 버전
/*
var connect = require('../node_modules/connect');

connect.createServer(
    connect.static("../test")
).listen(5000);
*/

//3.x 버전
// serve-static 설치해야함 (npm install serve-static (intellij,webstorm) project라면, setting에서 node package install
var connect = require('../node_modules/connect'),
    serverStatic = require('../node_modules/serve-static')

var app = connect();

app.use(serverStatic("../src"));
app.listen(5000);
